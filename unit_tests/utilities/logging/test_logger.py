from logging import Formatter, Logger
from logging.handlers import RotatingFileHandler

from src.utilities.logging.logger import log_error
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import LOGGER
from unittest.mock import patch, MagicMock, Mock, mock_open


class TestLogger(MTestCase):

    @patch("builtins.open", new_callable=mock_open)
    @patch(LOGGER.ROTATING_FILE_HANDLER, autospec=True)
    @patch(LOGGER.LOGGING, autospec=True)
    @patch(LOGGER.CONFIGURATION, autospec=True)
    def test_log(self, mock_configuration: MagicMock, mock_logging: MagicMock, mock_handler_class: MagicMock, _):
        mock_configuration.return_value.log_file_path = log_file_path = "somewhere/over/the/rainbow.txt"
        error_ = Exception("boom")
        mock_logging.Formatter.return_value = mock_formatter = Mock(spec=Formatter)
        mock_handler_class.return_value = mock_handler = Mock(spec=RotatingFileHandler)
        mock_logging.getLogger.return_value = mock_logger = Mock(spec=Logger)

        log_error("module name", error_)
        
        mock_logging.Formatter.assert_called_once_with("[%(asctime)s : %(name)s : %(message)s")
        mock_handler_class.assert_called_once_with(log_file_path, mode='a', maxBytes=5*1024*1024, delay=0)
        mock_handler.setFormatter.assert_called_once_with(mock_formatter)
        
        mock_logging.getLogger.assert_called_once_with("module name")
        mock_logger.addHandler.assert_called_once_with(mock_handler)
        mock_logger.exception.assert_called_once_with(error_)


