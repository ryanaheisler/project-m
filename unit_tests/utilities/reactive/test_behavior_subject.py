from src.utilities.reactive.subject import Subject, BehaviorSubject
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.testing_observer import TestingObserver


class TestBehaviorSubject(MTestCase):

    def test_is_subject(self):
        self.assertIsInstance(BehaviorSubject("anything"), Subject)

    def test_notifies_multiple_subscribers_and_replays_most_recent_value(self):
        subject = BehaviorSubject("hello")

        observer1 = TestingObserver()
        subject.subscribe(observer1.observe)

        self.assertListEqual(["hello"], observer1.values)

        subject.next(1)
        subject.next([1, 2, 3])

        observer2 = TestingObserver()
        subject.subscribe(observer2.observe)

        self.assertListEqual(["hello", 1, [1, 2, 3]], observer1.values)
        self.assertListEqual([[1, 2, 3]], observer2.values)

    def test_get_current_value_without_subscribing(self):
        subject = BehaviorSubject('yellow')

        self.assertEqual("yellow", subject.current_value)

        subject.next('cyan')

        self.assertEqual("cyan", subject.current_value)
