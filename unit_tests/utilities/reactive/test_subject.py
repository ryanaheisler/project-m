from src.utilities.reactive.subject import Subject
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.testing_observer import TestingObserver


class TestSubject(MTestCase):

    def test_notifies_multiple_subscribers(self):
        subject = Subject()

        observer1 = TestingObserver()
        subject.subscribe(observer1.observe)

        subject.next("hello")

        self.assertListEqual(["hello"], observer1.values)

        observer2 = TestingObserver()
        subject.subscribe(observer2.observe)

        subject.next(1)
        subject.next([1, 2, 3])

        self.assertListEqual(["hello", 1, [1, 2, 3]], observer1.values)
        self.assertListEqual([1, [1, 2, 3]], observer2.values)

    def test_prunes_dead_observers_on_next(self):
        observer = TestingObserver()
        subject = Subject()
        subject.subscribe(observer.observe)

        del observer
        self.assertEqual(1, len(subject._SelfPruningSubject__observers))

        subject.next("anything")
        self.assertEqual(0, len(subject._SelfPruningSubject__observers))

    def test_prunes_dead_observers_on_subscribe(self):
        observer1 = TestingObserver()
        observer2 = TestingObserver()
        subject = Subject()
        subject.subscribe(observer1.observe)

        del observer1
        self.assertEqual(1, len(subject._SelfPruningSubject__observers))

        subject.subscribe(observer2.observe)
        self.assertEqual(1, len(subject._SelfPruningSubject__observers))
        
        subject.next("anything")
        self.assertListEqual(["anything"], observer2.values)
