from typing import Callable
from unittest.mock import Mock
from src.utilities.command import Command
from unit_tests.test_utils.m_test_case import MTestCase


class TestCommand(MTestCase):

    def test_is_Callable(self):
        self.assertIsInstance(Command(), Callable)

    def test_doesnt_explode_with_no_instructions(self):
        try:
            Command()()
        except Exception as e:
            self.fail("Command blew up: %s" % e)

    def test_runs_instructions(self):
        instruction1 = Mock()
        instruction2 = Mock()

        command = Command([instruction1, instruction2])

        command()

        instruction1.assert_called_once_with()
        instruction2.assert_called_once_with()

    def test_add_instructions(self):
        instruction1 = Mock()
        instruction2 = Mock()
        instruction3 = Mock()

        command = Command([instruction1])

        command.add_instructions([instruction2, instruction3])

        command()

        instruction1.assert_called_once_with()
        instruction2.assert_called_once_with()
        instruction3.assert_called_once_with()
