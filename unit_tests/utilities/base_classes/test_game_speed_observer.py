from src.utilities.base_classes.game_speed_observer import GameSpeedObserver
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.mock_configuration import MockConfiguration
from unit_tests.test_utils.patchable_modules import GAME_SPEED_OBSERVER
from unittest.mock import patch, MagicMock


class TestGameSpeedObserver(MTestCase):

    def test_is_abstract_base_class(self):
        with self.assertRaises(TypeError) as context_manager:
            GameSpeedObserver()

        self.assertIn(
            "Can't instantiate abstract class GameSpeedObserver with abstract methods ", context_manager.exception.args[0]
        )

    @patch(GAME_SPEED_OBSERVER.CONFIGURATION, autospec=True)
    def test_subscribes_to_game_speed_changes_with_abstract_method(self, mock_configuration_class: MagicMock):
        initial_game_speed = 100
        new_speed = 45
        mock_configuration_class.return_value = mock_configuration = MockConfiguration(game_speed=initial_game_speed)
        animations = AnimationsSubclass()

        self.assertEqual(initial_game_speed, animations.game_speed)

        mock_configuration.game_speed = new_speed

        self.assertEqual(new_speed, animations.game_speed)


class AnimationsSubclass(GameSpeedObserver):
    def __init__(self):
        self.game_speed = None
        super().__init__()

    def update_game_speed(self, speed: int):
        self.game_speed = speed
