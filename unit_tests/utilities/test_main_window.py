from unit_tests.test_utils.patchable_modules import MAIN_WINDOW
from unittest.mock import patch, Mock, MagicMock

from pyglet.window import Window

from src.utilities.main_window import main_window
from unit_tests.test_utils.m_test_case import MTestCase


class TestMainWindow(MTestCase):
    @patch(MAIN_WINDOW.WINDOW)
    def test_creates_window(self, mock_pyglet_window: MagicMock):

        singleton_window: Mock = Mock(spec=Window)
        mock_pyglet_window.side_effect = [singleton_window, Mock(spec=Window)]
        window = main_window()
        self.assertIs(window, main_window())
        self.assertIs(singleton_window, window)
        mock_pyglet_window.assert_called_once_with(fullscreen=False)
