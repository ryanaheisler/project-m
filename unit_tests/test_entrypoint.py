from typing import Callable

from src.entrypoint import main
from src.utilities.resources import application_resources
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import ENTRYPOINT
from unit_tests.test_utils.verifiable_mock import VerifiableMock
from unittest.mock import patch, MagicMock


@patch(ENTRYPOINT.GAME_STATE_MANAGER)
@patch(ENTRYPOINT.PYGLET)
class TestGameStateManager(MTestCase):

    def test_sets_the_resource_path_for_pyglet(self, mock_pyglet: MagicMock, mock_game_state_manager: MagicMock):
        mock_reindex = VerifiableMock(spec=Callable)

        def check_resource_path():
            self.assertEqual(application_resources, mock_pyglet.resource.path)

        mock_reindex.side_effect = check_resource_path
        mock_pyglet.resource.reindex = mock_reindex

        def check_resources_already_reindexed():
            mock_reindex.assert_called_once_with()

        mock_game_state_manager.side_effect = check_resources_already_reindexed

        mock_run = VerifiableMock(spec=Callable)

        def check_game_state_manager_started():
            mock_game_state_manager.assert_called_once_with()

        mock_run.side_effect = check_game_state_manager_started
        mock_pyglet.app.run = mock_run

        main()

        mock_run.assert_called_once_with()

    @patch(ENTRYPOINT.LOG_ERROR)
    def test_logs_uncaught_errors(self, mock_log_error: MagicMock, mock_pyglet: MagicMock, mock_game_state_manager: MagicMock):
        error_ = Exception("boom")

        mock_pyglet.resource.reindex.side_effect = error_
        main()
        mock_log_error.assert_called_once_with("UNCAUGHT", error_)
        mock_pyglet.reset_mock()
        mock_log_error.reset_mock()

        mock_game_state_manager.side_effect = error_
        main()
        mock_log_error.assert_called_once_with("UNCAUGHT", error_)
        mock_game_state_manager.reset_mock()
        mock_log_error.reset_mock()

        mock_pyglet.app.run.side_effect = error_
        main()
        mock_log_error.assert_called_once_with("UNCAUGHT", error_)
