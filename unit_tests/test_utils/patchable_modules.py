class CONFIGURATION(object):
    SELF_PATH = "src.configuration.configuration"
    OS = "src.configuration.configuration.os"
    RESOURCE = "src.configuration.configuration.resource"
    BEHAVIOR_SUBJECT = "src.configuration.configuration.BehaviorSubject"
    SINGLETON = "src.configuration.configuration.Singleton"
    JSON = "src.configuration.configuration.json"


class ENTRYPOINT(object):
    SELF_PATH = "src.entrypoint"
    PYGLET = "src.entrypoint.pyglet"
    GAME_STATE_MANAGER = "src.entrypoint.GameStateManager"
    LOG_ERROR = "src.entrypoint.log_error"
    APPLICATION_RESOURCES = "src.entrypoint.application_resources"


class GAME_PLAY(object):
    SELF_PATH = "src.game_state.game_play"
    DECIMAL = "src.game_state.game_play.Decimal"
    POINT = "src.game_state.game_play.Point"
    PLAYER = "src.game_state.game_play.Player"


class GAME_STATE_MANAGER(object):
    SELF_PATH = "src.game_state.game_state_manager"
    CLOCK = "src.game_state.game_state_manager.clock"
    RESOURCE = "src.game_state.game_state_manager.resource"
    PYGLET_WINDOW_MODULE = "src.game_state.game_state_manager.pyglet_window_module"
    BATCH = "src.game_state.game_state_manager.Batch"
    WINDOW = "src.game_state.game_state_manager.Window"
    CONFIGURATION = "src.game_state.game_state_manager.Configuration"
    GAME_PLAY = "src.game_state.game_state_manager.GamePlay"
    ON_KEY_RELEASE = "src.game_state.game_state_manager.on_key_release"
    ON_KEY_PRESS = "src.game_state.game_state_manager.on_key_press"
    GAME_SPEED_ON_KEY_PRESS = "src.game_state.game_state_manager.game_speed_on_key_press"
    COMMAND = "src.game_state.game_state_manager.Command"
    MAIN_WINDOW = "src.game_state.game_state_manager.main_window"
    SINGLETON = "src.game_state.game_state_manager.Singleton"


class POINT(object):
    SELF_PATH = "src.geometry.point"
    DECIMAL = "src.geometry.point.Decimal"
    NUMBER = "src.geometry.point.number"


class RECTANGLE(object):
    SELF_PATH = "src.geometry.rectangle"
    ANNOTATIONS = "src.geometry.rectangle.annotations"
    DECIMAL = "src.geometry.rectangle.Decimal"
    POINT = "src.geometry.rectangle.Point"


class CONTROLLER_MODEL(object):
    SELF_PATH = "src.input.controller_model"
    KEY = "src.input.controller_model.key"
    KEYBOARD_INPUT = "src.input.controller_model.KeyboardInput"
    SINGLETON = "src.input.controller_model.Singleton"


class INPUT_HANDLER(object):
    SELF_PATH = "src.input.input_handler"
    EVENT_HANDLED = "src.input.input_handler.EVENT_HANDLED"
    KEY = "src.input.input_handler.key"
    CONTROLLER_MODEL = "src.input.input_handler.ControllerModel"
    KEYBOARD_INPUT = "src.input.input_handler.KeyboardInput"


class KEYBOARD_INPUT(object):
    SELF_PATH = "src.input.keyboard_input"
    ANNOTATIONS = "src.input.keyboard_input.annotations"


class GAME_SPEED_INPUT_HANDLER(object):
    SELF_PATH = "src.input.temporary.game_speed_input_handler"
    EVENT_HANDLED = "src.input.temporary.game_speed_input_handler.EVENT_HANDLED"
    KEY = "src.input.temporary.game_speed_input_handler.key"
    CONFIGURATION = "src.input.temporary.game_speed_input_handler.Configuration"


class PLAYER_ANIMATIONS(object):
    SELF_PATH = "src.player.animation.player_animations"
    RESOURCE = "src.player.animation.player_animations.resource"
    ANIMATION = "src.player.animation.player_animations.Animation"
    IMAGE_GRID = "src.player.animation.player_animations.ImageGrid"
    CENTERED_ANIMATION_FRAME = "src.player.animation.player_animations.CenteredAnimationFrame"
    GAME_SPEED_OBSERVER = "src.player.animation.player_animations.GameSpeedObserver"


class PLAYER_ANIMATOR(object):
    SELF_PATH = "src.player.animation.player_animator"
    ANIMATION = "src.player.animation.player_animator.Animation"
    PLAYER_ANIMATIONS = "src.player.animation.player_animator.PlayerAnimations"
    R_I_G_H_T = "src.player.animation.player_animator.RIGHT"
    S_T_A_T_I_O_N_A_R_Y = "src.player.animation.player_animator.STATIONARY"
    L_E_F_T = "src.player.animation.player_animator.LEFT"
    PLAYER_MOVEMENT_STATE = "src.player.animation.player_animator.PlayerMovementState"
    GAME_SPEED_OBSERVER = "src.player.animation.player_animator.GameSpeedObserver"


class PLAYER_MOVER(object):
    SELF_PATH = "src.player.movement.player_mover"
    ANNOTATIONS = "src.player.movement.player_mover.annotations"
    DECIMAL = "src.player.movement.player_mover.Decimal"
    TYPE_CHECKING = "src.player.movement.player_mover.TYPE_CHECKING"
    CONFIGURATION = "src.player.movement.player_mover.Configuration"
    PLAYER = "src.player.movement.player_mover.Player"
    R_I_G_H_T = "src.player.movement.player_mover.RIGHT"
    L_E_F_T = "src.player.movement.player_mover.LEFT"


class PLAYER_MOVEMENT_STATE(object):
    SELF_PATH = "src.player.movement_state.player_movement_state"
    ANNOTATIONS = "src.player.movement_state.player_movement_state.annotations"


class PLAYER_MOVEMENT_STATE_UPDATE_STRATEGIES(object):
    SELF_PATH = "src.player.movement_state.player_movement_state_update_strategies"
    CONTROLLER_MODEL = "src.player.movement_state.player_movement_state_update_strategies.ControllerModel"
    R_I_G_H_T = "src.player.movement_state.player_movement_state_update_strategies.RIGHT"
    L_E_F_T = "src.player.movement_state.player_movement_state_update_strategies.LEFT"
    PLAYER_MOVEMENT_STATE = "src.player.movement_state.player_movement_state_update_strategies.PlayerMovementState"


class PLAYER_MOVEMENT_STATE_UPDATER(object):
    SELF_PATH = "src.player.movement_state.player_movement_state_updater"
    CONTROLLER_MODEL = "src.player.movement_state.player_movement_state_updater.ControllerModel"
    PLAYER_MOVEMENT_STATE_UPDATE_STRATEGIES = "src.player.movement_state.player_movement_state_updater.player_movement_state_update_strategies"
    S_T_A_T_I_O_N_A_R_Y = "src.player.movement_state.player_movement_state_updater.STATIONARY"
    PLAYER_MOVEMENT_STATE = "src.player.movement_state.player_movement_state_updater.PlayerMovementState"


class PLAYER(object):
    SELF_PATH = "src.player.player"
    DECIMAL = "src.player.player.Decimal"
    BATCH = "src.player.player.Batch"
    SPRITE = "src.player.player.Sprite"
    POINT = "src.player.player.Point"
    PLAYER_ANIMATOR = "src.player.player.PlayerAnimator"
    MOVE_PLAYER = "src.player.player.move_player"
    PLAYER_MOVEMENT_STATE_UPDATER = "src.player.player.player_movement_state_updater"
    PLAYER_MOVEMENT_STATE = "src.player.player.PlayerMovementState"


class CENTERED_ANIMATION_FRAME(object):
    SELF_PATH = "src.pyglet_subclasses.centered_animation_frame"
    ANIMATION_FRAME = "src.pyglet_subclasses.centered_animation_frame.AnimationFrame"


class GAME_SPEED_OBSERVER(object):
    SELF_PATH = "src.utilities.base_classes.game_speed_observer"
    ABSTRACTMETHOD = "src.utilities.base_classes.game_speed_observer.abstractmethod"
    ABSTRACT_BASE_CLASS = "src.utilities.base_classes.game_speed_observer.AbstractBaseClass"
    CONFIGURATION = "src.utilities.base_classes.game_speed_observer.Configuration"


class COMMAND(object):
    SELF_PATH = "src.utilities.command"
    LIST = "src.utilities.command.List"
    CALLABLE = "src.utilities.command.Callable"


class LOGGER(object):
    SELF_PATH = "src.utilities.logging.logger"
    LOGGING = "src.utilities.logging.logger.logging"
    OS = "src.utilities.logging.logger.os"
    ROTATING_FILE_HANDLER = "src.utilities.logging.logger.RotatingFileHandler"
    CONFIGURATION = "src.utilities.logging.logger.Configuration"


class MAIN_WINDOW(object):
    SELF_PATH = "src.utilities.main_window"
    WINDOW = "src.utilities.main_window.Window"


class SUBJECT(object):
    SELF_PATH = "src.utilities.reactive.subject"
    WEAKREF = "src.utilities.reactive.subject.weakref"
    CALLABLE = "src.utilities.reactive.subject.Callable"


class RESOURCES(object):
    SELF_PATH = "src.utilities.resources"


class SINGLETON_META(object):
    SELF_PATH = "src.utilities.singleton_meta"


class TYPINGS(object):
    SELF_PATH = "src.utilities.typings"
    DECIMAL = "src.utilities.typings.Decimal"
    UNION = "src.utilities.typings.Union"


