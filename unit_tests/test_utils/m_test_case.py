import pyglet

from unit_tests.test_utils.helper_functions import reset_all_singleton_classes
from unit_tests.test_utils.resource_loader import ResourceLoaderForTests
from unittest import TestCase
from unittest.mock import patch, MagicMock


class MTestCase(TestCase):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        pyglet.resource.path = ['unit_tests/test_assets']
        pyglet.resource.reindex()

        self.addCleanup(reset_all_singleton_classes)

        self.resources = ResourceLoaderForTests()

    def patch(self, target: str, **kwargs) -> MagicMock:
        patcher = patch(target, **kwargs)
        mock = patcher.start()
        self.addCleanup(patcher.stop)
        return mock

    def assert_equality(self, object1, object2):
        self.assertNotEqual(object1, None)
        self.assertEqual(object1, object2)
        self.assertTrue(object1 == object2, msg="%s != %s" % (repr(object1), repr(object2)))
        self.assertEqual(object2, object1)
        self.assertTrue(object2 == object1, msg="%s != %s" % (repr(object2), repr(object1)))
        self.assertFalse(object1 != object2, msg="%s == %s" % (repr(object1), repr(object2)))
        self.assertFalse(object2 != object1, msg="%s == %s" % (repr(object2), repr(object1)))

    def assert_inequality(self, object1, object2):
        self.assertNotEqual(object1, None)
        self.assertNotEqual(object1, object2)
        self.assertTrue(object1 != object2, msg="%s == %s" % (repr(object1), repr(object2)))
        self.assertNotEqual(object2, object1)
        self.assertTrue(object2 != object1, msg="%s == %s" % (repr(object2), repr(object1)))
        self.assertFalse(object1 == object2, msg="%s != %s" % (repr(object1), repr(object2)))
        self.assertFalse(object2 == object1, msg="%s != %s" % (repr(object2), repr(object1)))

