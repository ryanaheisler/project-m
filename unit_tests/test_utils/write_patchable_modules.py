import os
import pkgutil

import re
import sys
from pathlib import Path

path_to_project_root: str = str(Path(os.getcwd()).parent.parent)
sys.path.append(str(path_to_project_root))


def _get_imported_modules(line_):
    things_being_imported = []
    space = " "
    words = line_.split(space)

    for word_index in range(-1, len(words) * -1, -1):
        if words[word_index] in ['import', 'as']:
            break
        else:
            things_being_imported.append(words[word_index])

    for index in range(len(things_being_imported[:])):

        if ',' in things_being_imported[index]:

            things_being_imported[index] = things_being_imported[index][:-1]

    return things_being_imported


modules_with_patchable_imports = []
patchable_modules = {}

for _, module_path, is_package in pkgutil.walk_packages(path=[path_to_project_root]):

    if not is_package and "src." in module_path and ".pyc" not in module_path:

        modules_with_patchable_imports.append(module_path)

for module_with_imports in modules_with_patchable_imports:

    paths_to_modules = {"SelfPath": module_with_imports}

    file_path = path_to_project_root + '/' + module_with_imports.replace('.', os.sep) + '.py'

    with open(file_path, 'r') as file_:

        lines = file_.readlines()

        for line_index in range(len(lines)):

            line = lines[line_index].strip()

            if len(line) > 0:

                if "import" in line:

                    imported_modules = _get_imported_modules(line)

                    for import_ in imported_modules:

                        if import_ != "*":

                            full_path_to_module = module_with_imports + "." + import_
                            elements_of_path = full_path_to_module.split(".")

                            paths_to_modules[elements_of_path[-1]] = ".".join([module_with_imports, import_])

    patchable_modules[module_with_imports.split(".")[-1]] = paths_to_modules

tab = "    "
with open('patchable_modules.py', 'w') as file_:

    for module in patchable_modules:

        if patchable_modules[module]:

            module_upper = module.upper()

            file_.write("class " + module_upper + "(object):" + os.linesep)

            for imported_module in patchable_modules[module]:

                imported_module_upper = imported_module.upper()

                if "_" not in imported_module:

                    imported_module_words = re.findall("[A-Z][^A-Z]*", imported_module)

                    for i in range(len(imported_module_words[:])):

                        imported_module_words[i] = imported_module_words[i].upper()

                    imported_module_upper = "_".join(imported_module_words)

                    if imported_module_upper == "":
                        imported_module_upper = imported_module.upper()

                file_.write(tab + imported_module_upper + " = \"" + patchable_modules[module][imported_module] + "\"" + os.linesep)

            file_.write(os.linesep)
            file_.write(os.linesep)
