class TestingObserver:
    def __init__(self):
        self.values = []

    def observe(self, value: any):
        self.values.append(value)
