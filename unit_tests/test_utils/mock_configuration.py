from src.configuration.configuration import Configuration
from src.utilities.reactive.subject import BehaviorSubject


class MockConfiguration(Configuration):
    def __init__(self, **kwargs):
        self.settings = kwargs
        self.__game_speed = BehaviorSubject(self.settings.get("game_speed") or 100)

    @property
    def should_show_frame_rate(self) -> bool:
        return self.settings.get("show_frame_rate") or True

    @property
    def game_speed(self) -> BehaviorSubject:
        return self.__game_speed

    @game_speed.setter
    def game_speed(self, value: int):
        self.__game_speed.next(value)

    @property
    def log_file_path(self) -> str:
        return self.settings.get("log_file_path") or "mock_configuration.txt"
