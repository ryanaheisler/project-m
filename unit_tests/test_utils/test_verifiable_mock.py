from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.verifiable_mock import VerifiableMock, Captor
from unittest.mock import Mock as Mock_


class TestVerifiableMock(MTestCase):

    def test_setup_and_returned_value(self):
        mock = VerifiableMock()
        self.assertIsInstance(mock, Mock_)

        setup_return_value = [1, 2, 3, 4]
        mock.expect_called_with("string", 1, f=3, g='3').and_return(setup_return_value)
        mock.expect_called_with("gloom").and_return("vileplume")

        self.assertIs(setup_return_value, mock("string", 1, f=3, g='3'))
        self.assertIs(setup_return_value, mock("string", 1, f=3, g='3'))
        self.assertIsNone(mock("stringg", 1, f=3, g='3'))
        self.assertIsNone(mock("string", 2, f=3, g='3'))
        self.assertIsNone(mock("string", 1, f=4, g='3'))
        self.assertIsNone(mock("string", 1, f=3, g='7'))

        self.assertEqual("vileplume", mock("gloom"))

    def test_setup_with_no_returned_value(self):
        mock = VerifiableMock()

        mock.expect_called_with("string", 1, f=3, g='3')

        self.assertIsNone(mock("string", 1, f=3, g='3'))

    def test_verify_returns_None_if_call_is_matched(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', three=4.5)
        mock('some other call')

        self.assertIsNone(mock.verify(1, '2', three=4.5))

    def test_verify_returns_None_if_call_is_matched_with_captor_arg(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', three=4.5)
        mock('some other call')

        captor = Captor()
        self.assertIsNone(captor.value)

        self.assertIsNone(mock.verify(1, captor, three=4.5))

        self.assertEqual('2', captor.value)

    def test_verify_fails_if_no_calls_match_args_length_with_arg_captor(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', three=4.5)

        with self.assertRaises(AssertionError) as error_context:
            self.assertIsNone(mock.verify(Captor()))

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t({captor}) \n  All calls to this mock: \n\t(1, '2', three=4.5)" % repr(mock)
        )

    def test_verify_fails_if_no_calls_match_args_length_with_kwarg_captor(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', three=4.5)

        with self.assertRaises(AssertionError) as error_context:
            self.assertIsNone(mock.verify(something=Captor()))

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t(something={captor}) \n  All calls to this mock: \n\t(1, '2', three=4.5)" % repr(mock)
        )

    def test_verify_returns_None_if_call_is_matched_with_captor_kwarg(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', three=4.5)
        mock('some other call', twenty=0)

        captor = Captor()
        self.assertIsNone(captor.value)

        self.assertIsNone(mock.verify(1, '2', three=captor))

        self.assertEqual(4.5, captor.value)

    def test_verify_fails_no_arguments_no_other_calls(self):
        mock = VerifiableMock(spec=str)

        with self.assertRaises(AssertionError) as error_context:
            mock.verify()

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t%s \n  All calls to this mock: \n\t<There were no calls to this mock>" % (repr(mock), '()')
        )

    def test_verify_fails_no_arguments_no_other_calls_with_captor(self):
        mock = VerifiableMock(spec=str)

        with self.assertRaises(AssertionError) as error_context:
            mock.verify(Captor(), cap=Captor())

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t%s \n  All calls to this mock: \n\t<There were no calls to this mock>"
            % (repr(mock), '({captor}, cap={captor})')
        )

    def test_verify_fails_no_arguments_but_other_calls(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', [3, 4, 5])
        mock("seven")

        with self.assertRaises(AssertionError) as error_context:
            mock.verify()

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t%s \n  All calls to this mock: \n\t%s\n\t%s" % (
                repr(mock),
                '()',
                "(1, '2', [3, 4, 5])",
                "('seven')"
            )
        )

    def test_verify_fails_with_arguments_and_other_calls(self):
        mock = VerifiableMock(spec=str)

        mock(1, '2', [3, 4, 5])
        mock()

        with self.assertRaises(AssertionError) as error_context:
            mock.verify("eight")

        error_message = error_context.exception.args[0]
        self.assertEqual(
            error_message,
            "Expected call not matched on %s: \n\t%s \n  All calls to this mock: \n\t%s\n\t%s" % (
                repr(mock),
                "('eight')",
                "(1, '2', [3, 4, 5])",
                "()"
            )
        )

    def test_verify_all_setups_returns_None_if_all_setups_are_matched(self):
        mock = VerifiableMock(spec=str)

        mock.expect_called_with(1, '2', three=4.5)
        mock.expect_called_with('some other call')

        mock(1, '2', three=4.5)
        mock('some other call')

        self.assertIsNone(mock.verify_expected_calls())

    def test_verify_all_setups_fails_if_setup_not_matched(self):
        mock = VerifiableMock(spec=str)

        mock.expect_called_with(1, '2', three=4.5)

        mock(1, '2', three=9.8)
        mock('another call')

        with self.assertRaises(AssertionError) as error_context:
            mock.verify_expected_calls()

        self.assertEqual(
            "Expected call not matched on %s: \n\t%s \n  All calls to this mock: \n\t%s\n\t%s" % (
                repr(mock),
                "(1, '2', three=4.5)",
                "(1, '2', three=9.8)",
                "('another call')"
            ),
            error_context.exception.args[0]
        )
