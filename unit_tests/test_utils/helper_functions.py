from typing import Callable

from parameterized import param

from src.utilities.singleton_meta import Singleton


def reset_all_singleton_classes():
    Singleton._Singleton__instances = {}


def parameterized_name_function(testcase_function: Callable, _parameter_number: str, parameters: param):
    return "%s_%s" % (testcase_function.__name__, parameters.args[0])


def parameterized_name_and_param_function(unique_param: int):
    def wrapped_function(testcase_function: Callable, _parameter_number: str, parameters: param):
        return "%s_%s (%s)" % (testcase_function.__name__, parameters.args[0], parameters.args[unique_param])

    return wrapped_function
