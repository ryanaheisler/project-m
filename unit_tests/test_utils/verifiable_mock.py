from typing import Any, List, Tuple, Dict
from unittest.mock import Mock as Mock_


class _MockSetup(object):
    def __init__(self, mock: Mock_, args: Tuple[Any], kwargs: Dict[Any, Any]):
        self.__mock: VerifiableMock = mock
        self._args = args
        self._kwargs = kwargs
        self._return_value = None

    def and_return(self, return_value: Any):
        self._return_value = return_value

    def _matches(self, args: Tuple[Any], kwargs: Dict[Any, Any]):
        return self._args == args and self._kwargs == kwargs


class Captor:
    def __init__(self):
        self.value = None

    def __str__(self):
        return "{captor}"

    def __repr__(self):
        return "{captor}"


class VerifiableMock(Mock_):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__setups: List[_MockSetup] = list()
        self.__actual_calls: List[Tuple[Tuple[Any], Dict[Any, Any]]] = list()

        def _side_effect(*args_, **kwargs_):
            self.__actual_calls.append((args_, kwargs_))
            matching_setup: _MockSetup = self.__get_matching_setup(args_, kwargs_)
            if matching_setup:
                return matching_setup._return_value

            return None

        self.side_effect = _side_effect

    def expect_called_with(self, *args, **kwargs):
        setup = _MockSetup(self, args, kwargs)
        self.__setups.append(setup)
        return setup

    def verify(self, *args, **kwargs):
        matching_call: Tuple[Tuple[Any], Dict[Any, Any]] = self.__get_matching_call(args, kwargs)

        if matching_call is None:
            raise AssertionError(
                "Expected call not matched on %s: \n\t%s \n" % (
                    repr(self), self.__get_string_call_arguments(args, kwargs)
                ) +
                "  All calls to this mock: \n" +
                "%s" % self.__get_all_calls_string(args, kwargs)
            )

    def verify_expected_calls(self):
        for setup in self.__setups:
            if self.__get_matching_call(setup._args, setup._kwargs) is None:
                raise AssertionError(
                    "Expected call not matched on %s: \n\t%s \n" % (
                        repr(self), self.__get_string_call_arguments(setup._args, setup._kwargs)
                    ) +
                    "  All calls to this mock: \n" +
                    "%s" % self.__get_all_calls_string(None, None)
                )

    def __get_matching_setup(self, args, kwargs) -> _MockSetup:
        for setup in self.__setups:
            if setup._matches(args, kwargs):
                return setup

    def __get_matching_call(self, args, kwargs) -> Tuple[Tuple[Any], Dict[Any, Any]]:

        if args == () and kwargs == {}:
            return next((call for call in self.__actual_calls if call == (args, kwargs)), None)

        filtered_list = [call for call in self.__actual_calls if len(call[0]) == len(args)]
        for index, arg in enumerate(args):
            if type(arg) != Captor:
                filtered_list = [call for call in filtered_list if len(call[0]) >= max(index - 1, 1) and call[0][index] == arg]
            # else:
            #     filtered_list = [call for call in filtered_list if len(call[0]) >= max(index - 1, 1)]

        for key in kwargs:
            value = kwargs[key]
            if type(value) != Captor:
                filtered_list = [call for call in filtered_list if call[1].get(key) == value]

        first_matching_call = next(iter(filtered_list), None)

        if first_matching_call:
            for index, arg in enumerate(args):
                if type(arg) == Captor:
                    captor: Captor = arg
                    captor.value = first_matching_call[0][index]

            for key in kwargs:
                value = kwargs[key]
                if type(value) == Captor:
                    captor: Captor = value
                    captor.value = first_matching_call[1][key]

        return first_matching_call

    @staticmethod
    def __get_string_call_arguments(args: Tuple[Any], kwargs: Dict[Any, Any]):
        positional_arguments = ", ".join([repr(arg) for arg in args])

        keyword_arguments = ", ".join(["%s=%s" % (key, repr(value)) for key, value in kwargs.items()])

        if positional_arguments == '':
            return "(%s)" % keyword_arguments
        elif keyword_arguments == '':
            return "(%s)" % positional_arguments
        else:
            return "(%s)" % ", ".join([positional_arguments, keyword_arguments])

    def __get_all_calls_string(self, args: Tuple[Any] or None, kwargs: Dict[Any, Any] or None):
        non_matching_calls = list(filter(lambda x: x != (args, kwargs), self.__actual_calls))
        if len(non_matching_calls) == 0:
            return "\t<There were no calls to this mock>"
        else:
            other_calls = []
            for call in non_matching_calls:
                other_calls.append("\t%s" % self.__get_string_call_arguments(call[0], call[1]))
            return "\n".join(other_calls)
