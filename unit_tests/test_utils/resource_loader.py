import os

from pyglet.image import Animation, AnimationFrame, ImageGrid
from pyglet.resource import Loader


class ResourceLoaderForTests(Loader):
    def __init__(self):
        super().__init__(path=['assets', 'unit_tests/test_assets'], script_home=os.getcwd())
        self.sample_image = self.image('1x1.png')
        self.sample_animation = self.get_sample_animation()

    def get_sample_animation(self):
        image = self.image('1x1.png')
        grid = ImageGrid(image, rows=1, columns=1, item_width=1, item_height=1)
        frames = [AnimationFrame(grid[0], 0.2)]
        return Animation(frames)
