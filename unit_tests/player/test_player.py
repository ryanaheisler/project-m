from decimal import Decimal

from pyglet.graphics import Batch

from src.geometry.point import Point
from src.player.player import Player
from src.player.movement_state.player_movement_state import PlayerMovementState
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import PLAYER
from unittest.mock import patch, MagicMock, Mock


class TestPlayer(MTestCase):

    def setUp(self) -> None:
        self.mock_player_animator = self.patch(PLAYER.PLAYER_ANIMATOR, autospec=True)
        self.get_next_animation = self.mock_player_animator.return_value.get_animation_for_state
        self.get_next_animation.return_value = self.resources.get_sample_animation()
        
        self.get_initial_animation = self.mock_player_animator.return_value.get_initial_animation
        self.get_initial_animation.return_value = self.resources.get_sample_animation()

        self.delta_time = Decimal(1.0)

    def test_position(self):
        player = Player(sprite_batch=Batch(), position=Point(150, 37))
        self.assertEqual(150, player.x)
        self.assertIsInstance(player.x, int)
        self.assertEqual(37, player.y)
        self.assertIsInstance(player.y, int)

        player = Player(sprite_batch=Batch())
        self.assertEqual(0, player.x)
        self.assertEqual(0, player.y)

    def test_loads_idle_animation(self):
        batch = Batch()

        player = Player(sprite_batch=batch)

        self.assertIs(self.get_initial_animation.return_value, player.image)
        self.assertIs(batch, player.batch)

        self.mock_player_animator.assert_called_once_with(PlayerMovementState())

    @patch(PLAYER.MOVE_PLAYER, autospec=True)
    @patch(PLAYER.PLAYER_MOVEMENT_STATE_UPDATER, autospec=True)
    @patch(PLAYER.PLAYER_MOVEMENT_STATE, autospec=True)
    def test_tick(self, mock_state: MagicMock, mock_state_updater: MagicMock, mock_move_player: MagicMock):
        mock_state_updater.update = mock_update = Mock(spec=PlayerMovementState)

        player = Player(sprite_batch=Batch())
        self.assertIs(mock_state.return_value, player.movement_state)
        mock_state.assert_called_once_with()
        mock_update.assert_not_called()
        self.get_next_animation.reset_mock()

        player.tick(self.delta_time)

        mock_update.assert_called_once_with(mock_state.return_value)
        self.assertIs(mock_update.return_value, player.movement_state)

        self.get_next_animation.assert_called_once_with(player.movement_state)
        self.assertIs(self.get_next_animation.return_value, player.image)

        mock_move_player.assert_called_once_with(player, self.delta_time)

    @patch(PLAYER.PLAYER_MOVEMENT_STATE_UPDATER, autospec=True)
    @patch(PLAYER.PLAYER_MOVEMENT_STATE, autospec=True)
    def test_tick_doesnt_reassign_animation_if_it_is_none(self, _mock_state: MagicMock, _mock_state_updater: MagicMock):
        player = Player(sprite_batch=Batch())
        original_animation = player.image

        self.get_next_animation.reset_mock()
        self.get_next_animation.return_value = None

        player.tick(self.delta_time)

        self.get_next_animation.assert_called_once_with(player.movement_state)
        self.assertIs(original_animation, player.image)
