from src.input.controller_model import ControllerModel
from src.player.movement_state import player_movement_state_update_strategies
from src.player.movement_state.player_movement_state import PlayerMovementState, LEFT, STATIONARY, RIGHT
from unit_tests.test_utils.m_test_case import MTestCase


class TestPlayerMovementStateUpdaterRight(MTestCase):
    def test_right_neutral_left(self):
        state1 = PlayerMovementState(vertical=894324)

        state2 = player_movement_state_update_strategies.right(state1)
        self.assertEqual(RIGHT, state2.horizontal)
        self.assertEqual(state1.vertical, state2.vertical)
        self.assertIsNot(state2, state1)

    def test_right_player_already_moving_left(self):
        ControllerModel().left = True
        state1 = PlayerMovementState(horizontal=LEFT)
        state2 = player_movement_state_update_strategies.right(state1)
        self.assertIs(state2, state1)

    def test_left_player_already_moving_right_but_right_released(self):
        ControllerModel().left = False
        state1 = PlayerMovementState(horizontal=LEFT)

        state2 = player_movement_state_update_strategies.right(state1)

        self.assertEqual(RIGHT, state2.horizontal)
        self.assertEqual(state1.vertical, state2.vertical)
        self.assertIsNot(state2, state1)
