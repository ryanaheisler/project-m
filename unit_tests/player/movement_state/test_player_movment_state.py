from src.player.movement_state.player_movement_state import PlayerMovementState, STATIONARY, ON_GROUND
from unit_tests.test_utils.m_test_case import MTestCase


class TestPlayerMovementState(MTestCase):
    def test_stores_horizontal_and_vertical_states(self):
        state = PlayerMovementState()

        self.assertEqual(STATIONARY, state.horizontal)
        self.assertEqual(ON_GROUND, state.vertical)

        state = PlayerMovementState(horizontal=819, vertical=489)

        self.assertEqual(819, state.horizontal)
        self.assertEqual(489, state.vertical)

    def test_equality(self):
        state0 = PlayerMovementState(1, 3)
        state1 = PlayerMovementState(1, 3)
        state2 = PlayerMovementState(2, 3)
        state3 = PlayerMovementState(1, 4)
        state4 = PlayerMovementState(6, 7)

        self.assert_equality(state0, state1)
        self.assert_inequality(state0, state2)
        self.assert_inequality(state0, state3)
        self.assert_inequality(state0, state4)
