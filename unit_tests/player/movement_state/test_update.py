from src.input.controller_model import ControllerModel
from src.player.movement_state import player_movement_state_updater
from src.player.movement_state.player_movement_state import PlayerMovementState, STATIONARY
from src.player.movement_state.player_movement_state_updater import update
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import PLAYER_MOVEMENT_STATE_UPDATER
from unittest.mock import patch, MagicMock


class TestMovementStateUpdater(MTestCase):
    @patch(PLAYER_MOVEMENT_STATE_UPDATER.PLAYER_MOVEMENT_STATE_UPDATE_STRATEGIES, autospec=True)
    def test_update_calls_movement_state_strategies_horizontal(self, mock_strategies: MagicMock):
        state1 = PlayerMovementState()

        controller = ControllerModel()
        controller.left = True
        controller.right = True

        new_state = player_movement_state_updater.update(state1)
        mock_strategies.left.assert_called_once_with(state1)
        mock_strategies.right.assert_called_once_with(mock_strategies.left.return_value)

        self.assertIs(mock_strategies.right.return_value, new_state)

    def test_returns_neutral_horizontal_state_if_no_buttons_pressed(self):
        state1 = PlayerMovementState(horizontal=4189)

        state2 = update(state1)
        self.assertEqual(STATIONARY, state2.horizontal)
        self.assertEqual(state1.vertical, state2.vertical)
        self.assertIsNot(state2, state1)

