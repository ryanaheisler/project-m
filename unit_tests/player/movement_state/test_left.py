from src.input.controller_model import ControllerModel
from src.player.movement_state import player_movement_state_update_strategies
from src.player.movement_state.player_movement_state import PlayerMovementState, LEFT, STATIONARY, RIGHT
from unit_tests.test_utils.m_test_case import MTestCase


class TestPlayerMovementStateUpdaterLeft(MTestCase):
    def test_left_neutral_right(self):
        state1 = PlayerMovementState(vertical=894324)

        state2 = player_movement_state_update_strategies.left(state1)
        self.assertEqual(LEFT, state2.horizontal)
        self.assertEqual(state1.vertical, state2.vertical)
        self.assertIsNot(state2, state1)

    def test_left_player_already_moving_right(self):
        ControllerModel().right = True
        state1 = PlayerMovementState(horizontal=RIGHT)
        state2 = player_movement_state_update_strategies.left(state1)
        self.assertIs(state2, state1)

    def test_left_player_already_moving_right_but_right_released(self):
        ControllerModel().right = False
        state1 = PlayerMovementState(horizontal=RIGHT)

        state2 = player_movement_state_update_strategies.left(state1)

        self.assertEqual(LEFT, state2.horizontal)
        self.assertEqual(state1.vertical, state2.vertical)
        self.assertIsNot(state2, state1)
