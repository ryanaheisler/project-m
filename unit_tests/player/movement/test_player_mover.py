from decimal import Decimal

from parameterized import parameterized

from src.player.movement.player_mover import move_player
from src.player.movement_state.player_movement_state import PlayerMovementState, STATIONARY, LEFT, RIGHT
from src.player.player import Player
from unit_tests.test_utils.helper_functions import parameterized_name_and_param_function
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.mock_configuration import MockConfiguration
from unit_tests.test_utils.patchable_modules import PLAYER_MOVER
from unittest.mock import Mock


class TestPlayerMover(MTestCase):
    def test_doesnt_move_player_based_on_state(self):
        player = Mock(spec=Player)
        player.movement_state = PlayerMovementState(horizontal=STATIONARY)

        move_player(player, Decimal(1))

        player.update.assert_not_called()

    @parameterized.expand(
        [
            ("left", LEFT, Decimal(1000.0), -360),
            ("left", LEFT, Decimal(500.0), -180),
            ("left", LEFT, Decimal(250.0), -90),
            ("left", LEFT, Decimal(64.0), -23),
            ("left", LEFT, Decimal(48.0), -17),
            ("left", LEFT, Decimal(32.0), -12),
            ("left", LEFT, Decimal(16.0), -6),
            ("right", RIGHT, Decimal(1000.0), 360),
            ("right", RIGHT, Decimal(500.0), 180),
            ("right", RIGHT, Decimal(250.0), 90),
            ("right", RIGHT, Decimal(64.0), 23),
            ("right", RIGHT, Decimal(48.0), 17),
            ("right", RIGHT, Decimal(32.0), 12),
            ("right", RIGHT, Decimal(16.0), 6),
        ],
        name_func=parameterized_name_and_param_function(2)
    )
    def test_moves_player_based_on_delta_time_game_speed_100_percent(self, _name, direction, delta_milliseconds, distance):
        mock_configuration = MockConfiguration()
        self.patch(PLAYER_MOVER.CONFIGURATION, new=lambda: mock_configuration)
        mock_configuration.game_speed = 100

        player = Mock(spec=Player)
        player.movement_state = PlayerMovementState(horizontal=direction)
        player.x = 34

        move_player(player, delta_milliseconds)

        player.update.assert_called_once_with(x=player.x + distance)

    @parameterized.expand(
        [
            ("left", LEFT, Decimal(1000.0), -90),
            ("left", LEFT, Decimal(500.0), -45),
            ("left", LEFT, Decimal(250.0), -22.5),
            ("left", LEFT, Decimal(64.0), -5.75),
            ("left", LEFT, Decimal(48.0), -4.25),
            ("left", LEFT, Decimal(32.0), -3),
            ("left", LEFT, Decimal(17.0), -1.5),
            ("right", RIGHT, Decimal(1000.0), 90),
            ("right", RIGHT, Decimal(500.0), 45),
            ("right", RIGHT, Decimal(250.0), 22.5),
            ("right", RIGHT, Decimal(64.0), 5.75),
            ("right", RIGHT, Decimal(48.0), 4.25),
            ("right", RIGHT, Decimal(32.0), 3),
            ("right", RIGHT, Decimal(17.0), 1.5),
        ],
        name_func=parameterized_name_and_param_function(2)
    )
    def test_moves_player_based_on_delta_time_game_speed_25_percent(self, _name, direction, delta_milliseconds, distance):
        mock_configuration = MockConfiguration()
        self.patch(PLAYER_MOVER.CONFIGURATION, new=lambda: mock_configuration)
        mock_configuration.game_speed = 25

        player = Mock(spec=Player)
        player.movement_state = PlayerMovementState(horizontal=direction)
        player.x = 25

        move_player(player, delta_milliseconds)

        player.update.assert_called_once_with(x=player.x + round(distance))
