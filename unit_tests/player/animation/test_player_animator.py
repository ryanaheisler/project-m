from parameterized import parameterized
from pyglet.image import Animation

from src.player.animation.player_animator import PlayerAnimator
from src.player.movement_state.player_movement_state import PlayerMovementState, LEFT, RIGHT, STATIONARY
from src.utilities.base_classes.game_speed_observer import GameSpeedObserver
from unit_tests.test_utils.helper_functions import parameterized_name_function
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.mock_configuration import MockConfiguration
from unit_tests.test_utils.patchable_modules import PLAYER_ANIMATOR, GAME_SPEED_OBSERVER
from unittest.mock import patch, MagicMock


@patch(PLAYER_ANIMATOR.PLAYER_ANIMATIONS)
class TestPlayerAnimator(MTestCase):

    def setUp(self) -> None:
        self.mock_configuration = MockConfiguration()
        self.patch(GAME_SPEED_OBSERVER.CONFIGURATION, new=lambda: self.mock_configuration)

    def test_is_game_speed_observer(self, _a):
        self.assertIsInstance(PlayerAnimator(PlayerMovementState(horizontal=53, vertical=39)), GameSpeedObserver)

    def test_returns_none_if_state_is_unchanged(self, _a):
        animator = PlayerAnimator(PlayerMovementState(horizontal=53, vertical=39))
        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=53, vertical=39))
        self.assertIsNone(animation)

    def test_returns_new_animation_once_when_game_speed_changes(self, mock_animations: MagicMock):
        run_left = "idle left one"
        idle_left = "idle left two"
        mock_animations.return_value.run_left = run_left
        animator = PlayerAnimator(PlayerMovementState(horizontal=LEFT))
        animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))

        mock_animations.return_value.idle_left = idle_left
        self.mock_configuration.game_speed = 45

        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))
        self.assertIs(idle_left, animation)

        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))
        self.assertIsNone(animation)

        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))
        self.assertIsNone(animation)

    def test_has_method_to_get_initial_animation(self, mock_animations: MagicMock):
        idle_right = "idle_right"
        mock_animations.return_value.idle_right = idle_right
        animator = PlayerAnimator(PlayerMovementState(horizontal=53, vertical=39))
        animation: Animation = animator.get_initial_animation()
        self.assertIs(idle_right, animation)

    def test_keeps_track_of_previous_state(self, mock_animations: MagicMock):
        animator = PlayerAnimator(PlayerMovementState(horizontal=STATIONARY))

        animation = animator.get_animation_for_state(PlayerMovementState(horizontal=LEFT))
        self.assertIs(mock_animations.return_value.run_left, animation)

        animation = animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))
        self.assertIs(mock_animations.return_value.idle_left, animation)

    @parameterized.expand(
        [
            ("left", LEFT, "run_left"),
            ("right", RIGHT, "run_right"),
        ],
        name_func=parameterized_name_function
    )
    def test_returns_correct_running_animation(self, mock_animations: MagicMock, _name, direction, animation_name):
        animator = PlayerAnimator(PlayerMovementState())
        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=direction))
        self.assertIs(getattr(mock_animations.return_value, animation_name), animation)

    @parameterized.expand(
        [
            ("left", PlayerMovementState(horizontal=LEFT), "idle_left"),
            ("right", PlayerMovementState(horizontal=RIGHT), "idle_right")
        ],
        name_func=parameterized_name_function
    )
    def test_returns_correct_idle_animation(
            self, mock_animations: MagicMock, _name, initial_state, animation_name
    ):
        animator = PlayerAnimator(initial_state)
        animation: Animation = animator.get_animation_for_state(PlayerMovementState(horizontal=STATIONARY))
        self.assertIs(getattr(mock_animations.return_value, animation_name), animation)
