from typing import Callable, List

from parameterized import parameterized
from pyglet.image import Animation

from src.player.animation.player_animations import PlayerAnimations
from src.pyglet_subclasses.centered_animation_frame import CenteredAnimationFrame
from src.utilities.base_classes.game_speed_observer import GameSpeedObserver
from unit_tests.test_utils.helper_functions import parameterized_name_function
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.mock_configuration import MockConfiguration
from unit_tests.test_utils.patchable_modules import PLAYER_ANIMATIONS, GAME_SPEED_OBSERVER
from unit_tests.test_utils.verifiable_mock import VerifiableMock
from unittest.mock import MagicMock, Mock


class TestPlayerAnimations(MTestCase):

    def setUp(self) -> None:
        self.sample_image = self.resources.sample_image
        self.sample_animation = self.resources.sample_animation
        self.mock_sample_animation = Mock(spec=Animation)

        self.mock_sample_animation.get_transform = VerifiableMock(spec=Callable)
        self.mock_sample_animation.get_transform.expect_called_with(flip_x=True).and_return(self.sample_animation)

        mock_resource: MagicMock = self.patch(PLAYER_ANIMATIONS.RESOURCE, autospec=True)
        self.mock_image_grid: VerifiableMock = self.patch(PLAYER_ANIMATIONS.IMAGE_GRID, new_callable=VerifiableMock)
        grid = [index for index in range(18)]
        self.mock_image_grid.expect_called_with(
            self.sample_image,
            rows=2,
            columns=9,
            item_width=56,
            item_height=96).and_return(grid)

        self.mock_animation_frame: VerifiableMock = self.patch(
            PLAYER_ANIMATIONS.CENTERED_ANIMATION_FRAME,
            new_callable=VerifiableMock
        )
        self.mock_animation: VerifiableMock = self.patch(PLAYER_ANIMATIONS.ANIMATION, new_callable=VerifiableMock)

        mock_image = VerifiableMock(spec=Callable, name="image")
        mock_image.expect_called_with('player.png').and_return(self.sample_image)
        mock_resource.image = mock_image

        self.mock_configuration = MockConfiguration()
        self.patch(GAME_SPEED_OBSERVER.CONFIGURATION, new=lambda: self.mock_configuration)

    def test_is_game_speed_observer(self):
        self.assertIsInstance(PlayerAnimations(), GameSpeedObserver)

    @parameterized.expand(
        [
            ("idle_right", [0, 1]),
            ("run_right", [4, 3, 2, 3]),
        ],
        name_func=parameterized_name_function
    )
    def test_has_animation(self, test_name, frame_numbers: List[int]):
        animation_frames = []
        mock_frames_by_index = {}
        for number in frame_numbers:
            default_new_frame = Mock(spec=CenteredAnimationFrame, name="frame %s" % number)
            frame = mock_frames_by_index.get(number, default_new_frame)
            animation_frames.append(frame)
            mock_frames_by_index[number] = frame
            self.mock_animation_frame.expect_called_with(number, 0.2).and_return(frame)

        self.mock_animation.expect_called_with(animation_frames).and_return(self.sample_animation)

        player_animations = PlayerAnimations()

        self.assertIs(self.sample_animation, getattr(player_animations, test_name))
        self.assertIs(getattr(player_animations, test_name), getattr(player_animations, test_name))

    @parameterized.expand(
        [
            ("idle_left", [0, 1]),
            ("run_left", [4, 3, 2, 3])
        ],
        name_func=parameterized_name_function
    )
    def test_has_flipped_animation(self, test_name, frame_numbers: List[int]):
        animation_frames = []
        mock_frames_by_index = {}
        for number in frame_numbers:
            default_new_frame = Mock(spec=CenteredAnimationFrame, name="frame %s" % number)
            frame = mock_frames_by_index.get(number, default_new_frame)
            animation_frames.append(frame)
            mock_frames_by_index[number] = frame
            self.mock_animation_frame.expect_called_with(number, 0.2).and_return(frame)

        self.mock_animation.expect_called_with(animation_frames).and_return(self.mock_sample_animation)

        player_animations = PlayerAnimations()

        self.assertIs(self.sample_animation, getattr(player_animations, test_name))
        self.assertIs(getattr(player_animations, test_name), getattr(player_animations, test_name))

    def test_resets_animations_when_game_speed_changes(self):
        animation_frames = []
        mock_frames_by_index = {}
        for number in [4, 3, 2, 3]:
            default_new_frame = Mock(spec=CenteredAnimationFrame, name="frame %s" % number)
            frame = mock_frames_by_index.get(number, default_new_frame)
            animation_frames.append(frame)
            mock_frames_by_index[number] = frame
            self.mock_animation_frame.expect_called_with(number, 0.2).and_return(frame)

        animation1 = self.resources.get_sample_animation()
        self.mock_animation.expect_called_with(animation_frames).and_return(animation1)

        player_animations = PlayerAnimations()

        self.assertIs(animation1, getattr(player_animations, "run_right"))
        self.assertIs(getattr(player_animations, "run_right"), getattr(player_animations, "run_right"))

        next_game_speed_percent = 50
        animation_frames = []
        mock_frames_by_index = {}
        for number in [4, 3, 2, 3]:
            default_new_frame = Mock(spec=CenteredAnimationFrame, name="frame %s" % number)
            frame = mock_frames_by_index.get(number, default_new_frame)
            animation_frames.append(frame)
            mock_frames_by_index[number] = frame
            self.mock_animation_frame.expect_called_with(number, 0.2 * (1 / (next_game_speed_percent / 100))).and_return(frame)

        animation2 = self.resources.get_sample_animation()
        self.mock_animation.expect_called_with(animation_frames).and_return(animation2)

        self.mock_configuration.game_speed.next(next_game_speed_percent)

        self.assertIs(animation2, getattr(player_animations, "run_right"))
        self.assertIs(getattr(player_animations, "run_right"), getattr(player_animations, "run_right"))
