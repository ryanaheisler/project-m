from decimal import Decimal

from src.geometry.point import Point
from src.geometry.rectangle import Rectangle
from unit_tests.test_utils.m_test_case import MTestCase


class TestRectangle(MTestCase):
    def test_attributes(self):
        bottom_left = Point(x=10, y=-10)
        rectangle = Rectangle(bottom_left=bottom_left, width=35, height=72)

        self.assertEqual(bottom_left, rectangle.position)
        self.assertEqual(35, rectangle.width)
        self.assertEqual(72, rectangle.height)

        self.assertEqual(10, rectangle.left)
        self.assertEqual(45, rectangle.right)
        self.assertEqual(-10, rectangle.bottom)
        self.assertEqual(62, rectangle.top)

        self.assertEqual(Point(10, -10), rectangle.bottom_left)
        self.assertEqual(Point(45, -10), rectangle.bottom_right)
        self.assertEqual(Point(10, 62), rectangle.top_left)
        self.assertEqual(Point(45, 62), rectangle.top_right)

        horizontal_midpoint = Decimal(35 / 2) + 10
        vertical_midpoint = 26
        self.assertEqual(Point(10, vertical_midpoint), rectangle.mid_left)
        self.assertEqual(Point(45, vertical_midpoint), rectangle.mid_right)
        self.assertEqual(Point(horizontal_midpoint, -10), rectangle.mid_bottom)
        self.assertEqual(Point(horizontal_midpoint, 62), rectangle.mid_top)

        self.assertEqual(Point(horizontal_midpoint, vertical_midpoint), rectangle.center)

    def test_collides_with(self):
        rectangle = Rectangle(Point(10, 10), 10, 10)

        collides0 = Rectangle(Point(9, 9), 12, 12)  # entirely enveloping
        collides1 = Rectangle(Point(10, 10), 1, 1)  # inside touching bottom-left
        collides2 = Rectangle(Point(19, 19), 1, 1)  # inside touching top-right
        collides3 = Rectangle(Point(9, 9), 2, 2)  # overlaps with bottom-left
        collides4 = Rectangle(Point(19, 19), 2, 2)  # overlaps with top-right
        collides5 = Rectangle(Point(12, 5), 1, 20)  # tall, skinny - both vertical sides cross top and bottom
        collides6 = Rectangle(Point(12, 5), 20, 20)  # tall, wide - one vertical side crosses top and bottom
        collides7 = Rectangle(Point(12, 15), 1, 20)  # tall, skinny - both vertical sides cross only top
        collides8 = Rectangle(Point(5, 12), 20, 1)  # wide, flat - both horizontal sides cross left and right
        collides9 = Rectangle(Point(5, 12), 20, 20)  # wide, tall - one horizontal side crosses left and right
        collides10 = Rectangle(Point(15, 12), 20, 1)  # wide, tall - both horizontal sides cross only right

        no_collision1 = Rectangle(Point(9, 9), 1, 1)  # touches bottom-left corner
        no_collision2 = Rectangle(Point(20, 20), 1, 1)  # touches top-right corner
        no_collision3 = Rectangle(Point(21, 10), 10, 10)  # 1 pixel to the right
        no_collision4 = Rectangle(Point(-1, 10), 10, 10)  # 1 pixel to the left
        no_collision5 = Rectangle(Point(10, 21), 10, 10)  # 1 pixel above
        no_collision6 = Rectangle(Point(10, -1), 10, 10)  # 1 pixel below

        self.assertTrue(rectangle.collides_with(rectangle))

        self.assertTrue(rectangle.collides_with(collides0))
        self.assertTrue(rectangle.collides_with(collides1))
        self.assertTrue(rectangle.collides_with(collides2))
        self.assertTrue(rectangle.collides_with(collides3))
        self.assertTrue(rectangle.collides_with(collides4))
        self.assertTrue(rectangle.collides_with(collides5))
        self.assertTrue(rectangle.collides_with(collides6))
        self.assertTrue(rectangle.collides_with(collides7))
        self.assertTrue(rectangle.collides_with(collides8))
        self.assertTrue(rectangle.collides_with(collides9))
        self.assertTrue(rectangle.collides_with(collides10))

        self.assertFalse(rectangle.collides_with(no_collision1))
        self.assertFalse(rectangle.collides_with(no_collision2))
        self.assertFalse(rectangle.collides_with(no_collision3))
        self.assertFalse(rectangle.collides_with(no_collision4))
        self.assertFalse(rectangle.collides_with(no_collision5))
        self.assertFalse(rectangle.collides_with(no_collision6))
