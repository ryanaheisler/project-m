from decimal import Decimal

from src.geometry.point import Point
from unit_tests.test_utils.m_test_case import MTestCase


class TestPoint(MTestCase):
    def test_attributes(self):
        point: Point = Point(x=Decimal(10), y=Decimal(-42))

        self.assertEqual(10, point.x)
        self.assertEqual(-42, point.y)

    def test_equality(self):
        point1: Point = Point(Decimal(12), Decimal(32))
        point2: Point = Point(x=12, y=32)
        point3: Point = Point(Decimal(11), Decimal(32.1))
        point4: Point = Point(Decimal(12.01), Decimal(31))

        self.assert_equality(point1, point2)
        self.assert_inequality(point1, point3)
        self.assert_inequality(point1, point4)

    def test_construct_with_integers(self):
        point1: Point = Point(1, 3)
        point2: Point = Point(Decimal(1), Decimal(3))

        self.assertEqual(point1, point2)

    def test_immutable(self):
        point: Point = Point(0, 0)

        try:
            point.x = 1
        except AttributeError as e:
            self.assertEqual("can't set attribute", e.args[0])

        try:
            point.y = 1
        except AttributeError as e:
            self.assertEqual("can't set attribute", e.args[0])
