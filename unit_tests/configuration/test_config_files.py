import json

from parameterized import parameterized

from unit_tests.test_utils.helper_functions import parameterized_name_function
from unit_tests.test_utils.m_test_case import MTestCase


class TestConfigFiles(MTestCase):

    @parameterized.expand([
        ("development", "support_files/config.json"),
        ("prototype", "build_support/config_files/prototype_config.json"),
    ],
        name_func=parameterized_name_function
    )
    def test_config_file_contains_correct_values(self, _name, path_to_file):
        with open(path_to_file, 'r') as config_file:
            configuration = json.load(config_file)

            self.assertIsInstance(configuration['show_frame_rate'], bool)

            self.assertIsInstance(configuration['game_speed'], int)
            self.assertGreater(configuration['game_speed'], 0)
            self.assertLessEqual(configuration['game_speed'], 100)

            self.assertIsInstance(configuration['log_file_path'], str)
            self.assertIn("~/", configuration['log_file_path'])
            self.assertIn(".txt", configuration['log_file_path'])

