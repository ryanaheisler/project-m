import os

from src.configuration.configuration import Configuration
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import CONFIGURATION
from unit_tests.test_utils.testing_observer import TestingObserver


class TestConfiguration(MTestCase):

    def setUp(self) -> None:
        self.patch(CONFIGURATION.RESOURCE, new=self.resources)

    def test_is_singleton(self):
        self.assertIs(Configuration(), Configuration())

    def test_show_frame_rate(self):
        config = Configuration()
        self.assertTrue(config.should_show_frame_rate)

    def test_game_speed(self):
        config = Configuration()

        observer = TestingObserver()
        config.game_speed.subscribe(observer.observe)

        self.assertListEqual([100], observer.values)

        config.game_speed = 58
        config.game_speed = 31

        self.assertListEqual([100, 58, 31], observer.values)

    def test_logging_file_location(self):
        config = Configuration()
        self.assertEqual(os.path.expanduser("~/hello.txt"), config.log_file_path)

