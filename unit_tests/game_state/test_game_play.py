from decimal import Decimal

from pyglet.graphics import Batch

from src.game_state.game_play import GamePlay
from src.geometry.point import Point
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import GAME_PLAY
from unittest.mock import patch, MagicMock, Mock


@patch(GAME_PLAY.PLAYER, autospec=True)
class TestGamePlay(MTestCase):

    def test_makes_sprite_and_adds_to_batch(self, mock_player: MagicMock):
        batch = Mock(spec=Batch)

        GamePlay(sprite_batch=batch)

        mock_player.assert_called_once_with(sprite_batch=batch, position=Point(100, 0))

    def test_tick(self, mock_player: MagicMock):
        batch = Mock(spec=Batch)

        play_state = GamePlay(sprite_batch=batch)

        delta_time = 1 / 60
        play_state.tick(delta_time)

        mock_player.return_value.tick.assert_called_once_with(Decimal(delta_time * 1000))
