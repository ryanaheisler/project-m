from pyglet.window import Window

from src.configuration.configuration import Configuration
from src.game_state.game_play import GamePlay
from src.game_state.game_state_manager import GameStateManager
from src.input.input_handler import on_key_press, on_key_release
from src.input.temporary.game_speed_input_handler import game_speed_on_key_press
from src.utilities.command import Command
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import GAME_STATE_MANAGER
from unit_tests.test_utils.verifiable_mock import VerifiableMock, Captor
from unittest.mock import patch, MagicMock, Mock


@patch(GAME_STATE_MANAGER.MAIN_WINDOW, autospec=True)
class TestGameStateManager(MTestCase):

    def setUp(self) -> None:
        self.mock_game_play_class = self.patch(GAME_STATE_MANAGER.GAME_PLAY, autospec=True)
        self.mock_game_play_class.return_value = self.mock_play_state = Mock(spec=GamePlay)

        self.mock_command_class = self.patch(GAME_STATE_MANAGER.COMMAND)
        self.mock_command_class.return_value = self.mock_command = Mock(spec=Command)

        self.mock_configuration_class = self.patch(GAME_STATE_MANAGER.CONFIGURATION)
        self.mock_configuration_class.return_value = self.mock_configuration = Mock(spec=Configuration)

    def test_is_singleton(self, _mw):
        self.assertIs(GameStateManager(), GameStateManager())

    @patch(GAME_STATE_MANAGER.CLOCK)
    def test_schedules_tick(self, mock_clock: MagicMock, _mw):
        GameStateManager()

        mock_clock.schedule_interval.assert_called_once_with(self.mock_play_state.tick, 1/60)

    @patch(GAME_STATE_MANAGER.BATCH, autospec=True)
    def test_passes_batch_to_game_play(self, mock_batch: MagicMock, _mock_main_window):
        GameStateManager()
        self.mock_game_play_class.assert_called_once_with(mock_batch.return_value)

    def test_pushes_input_handler_on_window(self, mock_main_window: MagicMock):
        mock_push_handlers: VerifiableMock = VerifiableMock(name="push_handlers")
        mock_window: Window = mock_main_window.return_value
        mock_window.push_handlers = mock_push_handlers

        GameStateManager()

        mock_push_handlers.verify(on_key_press, on_key_release)

    def test_pushes_temporary_input_handler_on_window(self, mock_main_window: MagicMock):
        mock_push_handlers: VerifiableMock = VerifiableMock(name="push_handlers")
        mock_window: Window = mock_main_window.return_value
        mock_window.push_handlers = mock_push_handlers

        GameStateManager()

        mock_push_handlers.verify(on_key_press=game_speed_on_key_press)

    @patch(GAME_STATE_MANAGER.PYGLET_WINDOW_MODULE, autospec=True)
    @patch(GAME_STATE_MANAGER.BATCH, autospec=True)
    def test_sets_command_as_ondraw_handler_with_FPS_display(
            self, mock_batch: MagicMock,
            mock_pyglet_window: MagicMock,
            mock_main_window: MagicMock):
        self.mock_configuration.should_show_frame_rate = True

        mock_window: Window = mock_main_window.return_value

        mock_fps: MagicMock = mock_pyglet_window.FPSDisplay.return_value

        GameStateManager()

        self.mock_command_class.assert_called_once_with([mock_window.clear, mock_batch.return_value.draw])
        self.mock_command.add_instructions.assert_called_once_with([mock_fps.draw])

        mock_pyglet_window.FPSDisplay.assert_called_once_with(window=mock_window)

        self.assertIs(mock_window.on_draw, self.mock_command)

    @patch(GAME_STATE_MANAGER.BATCH, autospec=True)
    def test_sets_command_as_ondraw_handler(
            self, mock_batch: MagicMock,
            mock_main_window: MagicMock):
        self.mock_configuration.should_show_frame_rate = False

        mock_window: Window = mock_main_window.return_value

        GameStateManager()

        self.mock_command_class.assert_called_once_with([mock_window.clear, mock_batch.return_value.draw])
        self.mock_command.add_instructions.assert_not_called()

        self.assertIs(mock_window.on_draw, self.mock_command)
