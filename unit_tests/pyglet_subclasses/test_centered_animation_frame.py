from pyglet.image import ImageGrid, AnimationFrame

from src.pyglet_subclasses.centered_animation_frame import CenteredAnimationFrame
from unit_tests.test_utils.m_test_case import MTestCase


class TestCenteredAnimationFrame(MTestCase):
    def test_sets_anchor_x(self):
        grid = ImageGrid(self.resources.sample_image, rows=1, columns=4, item_width=40, item_height=1)
        frame = CenteredAnimationFrame(grid[2], 1)

        self.assertIsInstance(frame, AnimationFrame)
        self.assertEqual(20, frame.image.anchor_x)
