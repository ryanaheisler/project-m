from pyglet.window import key

from src.input.temporary.game_speed_input_handler import game_speed_on_key_press
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.mock_configuration import MockConfiguration
from unit_tests.test_utils.patchable_modules import GAME_SPEED_INPUT_HANDLER


class TestGameSpeedInputHandler(MTestCase):

    def setUp(self) -> None:
        self.mock_configuration = MockConfiguration()
        self.patch(GAME_SPEED_INPUT_HANDLER.CONFIGURATION, new=lambda: self.mock_configuration)
        self.game_speeds = []

    def game_speed_handler(self, speed: int):
        self.game_speeds.append(speed)

    def test_changes_game_speed_in_configuration(self):

        self.mock_configuration.game_speed.subscribe(self.game_speed_handler)
        self.assertListEqual([100], self.game_speeds)

        game_speed_on_key_press(key.MINUS)

        self.assertListEqual([100, 90], self.game_speeds)

        game_speed_on_key_press(key.MINUS)
        game_speed_on_key_press(key.MINUS)
        game_speed_on_key_press(key.MINUS)

        self.assertListEqual([100, 90, 80, 70, 60], self.game_speeds)

        game_speed_on_key_press(key.EQUAL)
        game_speed_on_key_press(key.EQUAL)
        game_speed_on_key_press(key.EQUAL)

        self.assertListEqual([100, 90, 80, 70, 60, 70, 80, 90], self.game_speeds)

    def test_doesnt_go_below_10_or_above_100_speed(self):
        self.mock_configuration.game_speed.subscribe(self.game_speed_handler)

        game_speed_on_key_press(key.EQUAL)
        game_speed_on_key_press(key.EQUAL)
        game_speed_on_key_press(key.EQUAL)

        self.assertListEqual([100], self.game_speeds)

        self.mock_configuration.game_speed = 10

        self.assertListEqual([100, 10], self.game_speeds)

        game_speed_on_key_press(key.MINUS)
        game_speed_on_key_press(key.MINUS)
        game_speed_on_key_press(key.MINUS)

        self.assertListEqual([100, 10], self.game_speeds)
