from parameterized import parameterized
from pyglet.window import key

from src.input.controller_model import ControllerModel
from src.input.keyboard_input import KeyboardInput
from unit_tests.test_utils.helper_functions import parameterized_name_function
from unit_tests.test_utils.m_test_case import MTestCase


class TestControllerModel(MTestCase):

    def test_is_singleton(self):
        self.assertIs(ControllerModel(), ControllerModel())

    @parameterized.expand(
        [
            ("left_key", "left", key.LEFT),
            ("right_key", "right", key.RIGHT),
        ],
        name_func=parameterized_name_function
    )
    def test_stores_input_for(self, _name, attribute, input_value):
        model = ControllerModel()

        self.assertFalse(getattr(model, attribute))

        model.update(KeyboardInput(input_value, is_pressed=True))
        self.assertTrue(getattr(model, attribute))

        model.update(KeyboardInput(input_value, is_pressed=False))
        self.assertFalse(getattr(model, attribute))

