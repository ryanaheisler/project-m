from pyglet.window import key

from src.input.keyboard_input import KeyboardInput
from unit_tests.test_utils.m_test_case import MTestCase


class TestKeyboardInput(MTestCase):
    def test_attributes(self):
        input_ = KeyboardInput(symbol=key.S, is_pressed=True)

        self.assertIs(key.S, input_.symbol)
        self.assertTrue(input_.is_pressed)

        input_ = KeyboardInput(symbol=key.ENTER, is_pressed=False)

        self.assertIs(key.ENTER, input_.symbol)
        self.assertFalse(input_.is_pressed)

    def test_equality(self):
        input0 = KeyboardInput(1, True)
        input1 = KeyboardInput(1, True)
        input2 = KeyboardInput(1, False)
        input3 = KeyboardInput(6, True)

        self.assert_equality(input0, input1)
        self.assert_inequality(input0, input2)
        self.assert_inequality(input0, input3)


