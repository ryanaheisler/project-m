from pyglet.event import EVENT_HANDLED
from pyglet.window import key

from src.input.input_handler import on_key_press, on_key_release
from src.input.keyboard_input import KeyboardInput
from unit_tests.test_utils.m_test_case import MTestCase
from unit_tests.test_utils.patchable_modules import INPUT_HANDLER
from unittest.mock import patch, MagicMock, call


@patch(INPUT_HANDLER.CONTROLLER_MODEL, autospec=True)
class TestInputHandler(MTestCase):
    def test_handles_key_press(self, mock_controller: MagicMock):
        result = on_key_press(symbol=key.X)
        self.assertIs(EVENT_HANDLED, result)

        result = on_key_press(symbol=key.LCTRL)
        self.assertIs(EVENT_HANDLED, result)

        mock_controller.return_value.update.assert_has_calls([
            call(KeyboardInput(key.X, True)),
            call(KeyboardInput(key.LCTRL, True)),
        ], any_order=False)

    def test_handles_key_release(self, mock_controller: MagicMock):
        result = on_key_release(symbol=key.X)
        self.assertIs(EVENT_HANDLED, result)

        result = on_key_release(symbol=key.LCTRL)
        self.assertIs(EVENT_HANDLED, result)

        mock_controller.return_value.update.assert_has_calls([
            call(KeyboardInput(key.X, False)),
            call(KeyboardInput(key.LCTRL, False)),
        ], any_order=False)

    def test_does_not_handle_escape(self, mock_controller: MagicMock):
        self.assertIsNone(on_key_press(symbol=key.ESCAPE))
        self.assertIsNone(on_key_release(symbol=key.ESCAPE))

        mock_controller.return_value.update.assert_not_called()
