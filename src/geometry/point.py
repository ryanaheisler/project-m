from decimal import Decimal

from src.utilities.typings import number


class Point:
    def __init__(self, x: number, y: number):

        self.__x: Decimal = Decimal(x)
        self.__y: Decimal = Decimal(y)

    @property
    def x(self) -> Decimal:
        return self.__x

    @property
    def y(self) -> Decimal:
        return self.__y

    def __eq__(self, other):
        return isinstance(other, Point) and self.x == other.x and self.y == other.y

    def __repr__(self):
        return "Point(%s, %s)" % (str(self.x), str(self.y))
