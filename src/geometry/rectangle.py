from __future__ import annotations

from decimal import Decimal

from src.geometry.point import Point


class Rectangle:
    def __init__(self, bottom_left: Point, width: int, height: int):
        self.position: Point = bottom_left
        self.width: int = width
        self.height: int = height

    def collides_with(self, other: Rectangle) -> bool:
        if self.left >= other.right or other.left >= self.right:
            return False

        if self.bottom >= other.top or other.bottom >= self.top:
            return False

        return True

    @property
    def left(self) -> Decimal:
        return self.position.x

    @property
    def right(self) -> Decimal:
        return self.position.x + self.width

    @property
    def bottom(self) -> Decimal:
        return self.position.y

    @property
    def top(self) -> Decimal:
        return self.position.y + self.height

    @property
    def bottom_left(self) -> Point:
        return self.position

    @property
    def mid_left(self) -> Point:
        return Point(self.left, self.__vertical_midpoint)

    @property
    def top_left(self) -> Point:
        return Point(self.left, self.top)

    @property
    def bottom_right(self) -> Point:
        return Point(self.right, self.bottom)

    @property
    def mid_right(self) -> Point:
        return Point(self.right, self.__vertical_midpoint)

    @property
    def top_right(self) -> Point:
        return Point(self.right, self.top)

    @property
    def mid_bottom(self) -> Point:
        return Point(self.__horizontal_midpoint, self.bottom)

    @property
    def mid_top(self) -> Point:
        return Point(self.__horizontal_midpoint, self.top)

    @property
    def center(self) -> Point:
        return Point(self.__horizontal_midpoint, self.__vertical_midpoint)

    @property
    def __horizontal_midpoint(self) -> Decimal:
        return self.left + Decimal(self.width / 2)

    @property
    def __vertical_midpoint(self) -> Decimal:
        return self.bottom + Decimal(self.height / 2)
