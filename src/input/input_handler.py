from pyglet.event import EVENT_HANDLED
from pyglet.window import key

from src.input.controller_model import ControllerModel
from src.input.keyboard_input import KeyboardInput


def on_key_press(symbol: int, _modifiers: int = None):
    if symbol != key.ESCAPE:
        ControllerModel().update(KeyboardInput(symbol, is_pressed=True))
        return EVENT_HANDLED


def on_key_release(symbol: int, _modifiers: int = None):
    if symbol != key.ESCAPE:
        ControllerModel().update(KeyboardInput(symbol, is_pressed=False))
        return EVENT_HANDLED
