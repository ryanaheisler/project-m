from __future__ import annotations


class KeyboardInput:

    def __init__(self, symbol, is_pressed):
        self.symbol: int = symbol
        self.is_pressed: bool = is_pressed

    def __eq__(self, other: KeyboardInput):
        return isinstance(other, KeyboardInput) and \
            other.symbol == self.symbol and \
            other.is_pressed is self.is_pressed

    def __repr__(self):
        return "%s(symbol=%s, is_pressed=%s)" %(
            self.__class__.__name__, self.symbol, self.is_pressed
        )
