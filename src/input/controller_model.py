from pyglet.window import key

from src.input.keyboard_input import KeyboardInput
from src.utilities.singleton_meta import Singleton


class ControllerModel(metaclass=Singleton):
    def __init__(self):
        self.left = False
        self.right = False

    def update(self, input_: KeyboardInput):
        if input_.symbol == key.LEFT:
            self.left = input_.is_pressed
        elif input_.symbol == key.RIGHT:
            self.right = input_.is_pressed
