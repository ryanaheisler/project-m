from pyglet.event import EVENT_HANDLED
from pyglet.window import key

from src.configuration.configuration import Configuration


def game_speed_on_key_press(symbol: int, _modifiers: int = None):
    current_speed = Configuration().game_speed.current_value
    if symbol == key.MINUS:
        if current_speed >= 20:
            Configuration().game_speed = current_speed - 10
        return EVENT_HANDLED
    elif symbol == key.EQUAL:
        if current_speed <= 90:
            Configuration().game_speed = current_speed + 10
        return EVENT_HANDLED
