from pyglet.image import Animation

from src.player.animation.player_animations import PlayerAnimations
from src.player.movement_state.player_movement_state import PlayerMovementState, LEFT, STATIONARY, RIGHT
from src.utilities.base_classes.game_speed_observer import GameSpeedObserver


class PlayerAnimator(GameSpeedObserver):

    def __init__(self, state: PlayerMovementState):
        super().__init__()
        self.__last_known_state = state
        self.__last_know_horizontal_direction = state.horizontal
        self.animations = PlayerAnimations()
        self.__game_speed_changed = False

    def get_initial_animation(self):
        return self.animations.idle_right

    def get_animation_for_state(self, state: PlayerMovementState) -> Animation or None:
        animation_to_return = None
        if state == self.__last_known_state and not self.__game_speed_changed:
            return animation_to_return
        elif state.horizontal == STATIONARY:
            animation_to_return = self.__stationary()
        elif state.horizontal == LEFT:
            self.__last_know_horizontal_direction = LEFT
            animation_to_return = self.animations.run_left
        else:
            self.__last_know_horizontal_direction = RIGHT
            animation_to_return = self.animations.run_right

        self.__last_known_state = state
        self.__game_speed_changed = False
        return animation_to_return

    def __stationary(self):
        if self.__last_known_state.horizontal == LEFT:
            return self.animations.idle_left
        elif self.__last_known_state.horizontal != RIGHT and self.__last_know_horizontal_direction == LEFT:
            return self.animations.idle_left

        return self.animations.idle_right

    def update_game_speed(self, speed: int):
        self.__game_speed_changed = True
