from pyglet import resource
from pyglet.image import ImageGrid, Animation

from src.pyglet_subclasses.centered_animation_frame import CenteredAnimationFrame
from src.utilities.base_classes.game_speed_observer import GameSpeedObserver


class PlayerAnimations(GameSpeedObserver):

    def __init__(self):
        super().__init__()

        image = resource.image('player.png')
        self.grid = ImageGrid(image, rows=2, columns=9, item_width=56, item_height=96)

        self.__clear_animations()
        self.__FRAME_LENGTH = 0.2

    def __get_animation_by_name(self, attribute_name, frame_numbers):
        private_attribute_name = '_%s__%s' % (self.__class__.__name__, attribute_name)
        if not getattr(self, private_attribute_name):
            frames = [CenteredAnimationFrame(self.grid[index], self.__animation_speed) for index in frame_numbers]
            setattr(self, private_attribute_name, Animation(frames))
        return getattr(self, private_attribute_name)

    def __get_flipped_animation(self, attribute_name, original_animation: Animation):
        private_attribute_name = '_%s__%s' % (self.__class__.__name__, attribute_name)
        if not getattr(self, private_attribute_name):
            setattr(self, private_attribute_name, original_animation.get_transform(flip_x=True))
        return getattr(self, private_attribute_name)

    @property
    def idle_right(self):
        return self.__get_animation_by_name("idle_right", [0, 1])

    @property
    def idle_left(self):
        return self.__get_flipped_animation("idle_left", self.idle_right)

    @property
    def run_right(self):
        return self.__get_animation_by_name("run_right", [4, 3, 2, 3])

    @property
    def run_left(self):
        return self.__get_flipped_animation("run_left", self.run_right)

    def update_game_speed(self, speed: int):
        self.__clear_animations()
        self.__game_speed = speed / 100

    @property
    def __animation_speed(self):
        return self.__FRAME_LENGTH * (1 / self.__game_speed)

    def __clear_animations(self):
        self.__idle_right = None
        self.__idle_left = None
        self.__run_right = None
        self.__run_left = None
