from decimal import Decimal

from pyglet.graphics import Batch
from pyglet.sprite import Sprite

from src.geometry.point import Point
from src.player.animation.player_animator import PlayerAnimator
from src.player.movement.player_mover import move_player
from src.player.movement_state import player_movement_state_updater
from src.player.movement_state.player_movement_state import PlayerMovementState


class Player(Sprite):

    def __init__(self, sprite_batch: Batch, position: Point = Point(0, 0)):
        self.movement_state = PlayerMovementState()

        self.animator = PlayerAnimator(self.movement_state)
        starting_animation = self.animator.get_initial_animation()

        super().__init__(starting_animation, batch=sprite_batch, x=int(position.x), y=int(position.y))

    def tick(self, delta_time_milliseconds: Decimal):
        self.movement_state = player_movement_state_updater.update(self.movement_state)
        next_animation = self.animator.get_animation_for_state(self.movement_state)
        if next_animation:
            self.image = next_animation

        move_player(self, delta_time_milliseconds)
