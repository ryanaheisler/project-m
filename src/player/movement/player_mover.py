from __future__ import annotations

from decimal import Decimal
from typing import TYPE_CHECKING

from src.configuration.configuration import Configuration

if TYPE_CHECKING:
    from src.player.player import Player

from src.player.movement_state.player_movement_state import LEFT, RIGHT


def move_player(player: Player, delta_time_milliseconds: Decimal):
    if player.movement_state.horizontal in [LEFT, RIGHT]:
        pixels_per_millisecond = Decimal(360/1000) * (Decimal(Configuration().game_speed.current_value / 100))
        horizontal_distance = delta_time_milliseconds * pixels_per_millisecond

        if player.movement_state.horizontal == LEFT:
            horizontal_distance *= -1

        new_position = round(float(horizontal_distance)) + player.x
        player.update(x=new_position)
