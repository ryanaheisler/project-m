from src.input.controller_model import ControllerModel
from src.player.movement_state.player_movement_state import PlayerMovementState, LEFT, RIGHT


def left(starting_state: PlayerMovementState) -> PlayerMovementState:
    if starting_state.horizontal != RIGHT or not ControllerModel().right:
        return PlayerMovementState(horizontal=LEFT, vertical=starting_state.vertical)
    return starting_state


def right(starting_state: PlayerMovementState) -> PlayerMovementState:
    if starting_state.horizontal != LEFT or not ControllerModel().left:
        return PlayerMovementState(horizontal=RIGHT, vertical=starting_state.vertical)
    return starting_state
