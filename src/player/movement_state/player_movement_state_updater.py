from src.input.controller_model import ControllerModel
from src.player.movement_state import player_movement_state_update_strategies
from src.player.movement_state.player_movement_state import PlayerMovementState, STATIONARY


def update(starting_state: PlayerMovementState) -> PlayerMovementState:
    controller = ControllerModel()
    if not controller.left and not controller.right:
        return PlayerMovementState(horizontal=STATIONARY, vertical=starting_state.vertical)
    else:
        state = starting_state
        if controller.left:
            state = player_movement_state_update_strategies.left(state)
        if controller.right:
            state = player_movement_state_update_strategies.right(state)
        return state
