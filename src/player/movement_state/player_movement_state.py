from __future__ import annotations

STATIONARY, LEFT, RIGHT = range(3)
ON_GROUND = 0


class PlayerMovementState:
    def __init__(self, horizontal=None, vertical=None):
        self.horizontal = horizontal or STATIONARY
        self.vertical = vertical or ON_GROUND

    def __repr__(self):
        return "%s(horizontal=%s, vertical=%s)" % (
            self.__class__.__name__,
            self.horizontal,
            self.vertical
        )

    def __eq__(self, other: PlayerMovementState) -> bool:
        return isinstance(other, PlayerMovementState) and \
            self.horizontal == other.horizontal and \
            self.vertical == other.vertical
