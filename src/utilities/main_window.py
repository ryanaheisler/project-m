from pyglet.window import Window

__window: Window or None = None


def main_window():
    global __window
    if not __window:
        __window = Window(fullscreen=False)
    return __window
