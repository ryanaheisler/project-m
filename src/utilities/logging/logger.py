import logging
import os
from logging.handlers import RotatingFileHandler

from src.configuration.configuration import Configuration


def log_error(module_name: str, error_: Exception):
    formatter = logging.Formatter("[%(asctime)s : %(name)s : %(message)s")
    file_path = Configuration().log_file_path

    handler = RotatingFileHandler(file_path, mode='a', maxBytes=5 * 1024 * 1024, delay=False)
    handler.setFormatter(formatter)

    logger = logging.getLogger(module_name)
    logger.addHandler(handler)

    logger.exception(error_)

    with open(file_path, 'a') as file_:
        file_.write(os.linesep)
