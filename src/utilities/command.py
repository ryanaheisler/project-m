from typing import Callable, List


class Command(Callable):

    def __init__(self, instructions: List[Callable] = None):
        self.__instructions = instructions or []

    def __call__(self, *args, **kwargs):
        for callable_ in self.__instructions:
            callable_()

    def add_instructions(self, instructions: List[Callable]):
        self.__instructions += instructions
