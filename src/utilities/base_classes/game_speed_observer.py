from abc import ABC as AbstractBaseClass, abstractmethod

from src.configuration.configuration import Configuration


class GameSpeedObserver(AbstractBaseClass):

    def __init__(self):
        subject = Configuration().game_speed
        subject.subscribe(self.update_game_speed)

    @abstractmethod
    def update_game_speed(self, speed: int):
        pass
