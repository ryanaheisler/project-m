from decimal import Decimal
from typing import Union

number = Union[int, float, Decimal]
