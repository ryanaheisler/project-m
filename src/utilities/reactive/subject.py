import weakref
from typing import Callable


class __SelfPruningSubject:
    def __init__(self):
        self.__observers = []

    def subscribe(self, observer):
        self.__prune()
        weak_observer = weakref.WeakMethod(observer)
        self.__observers.append(weak_observer)

    def next(self, value):
        self.__prune()
        for weak_observer in self.__observers:
            observer = weak_observer()
            observer(value)

    def __prune(self):
        for weak_observer in self.__observers:
            if weak_observer() is None:
                self.__unsubscribe(weak_observer)

    def __unsubscribe(self, weak_observer: weakref.WeakMethod):
        while weak_observer in self.__observers:
            self.__observers.remove(weak_observer)


class Subject(__SelfPruningSubject):
    pass


class BehaviorSubject(Subject):
    def __init__(self, initial_value):
        super().__init__()
        self.current_value = initial_value

    def subscribe(self, observer: Callable):
        super().subscribe(observer)
        observer(self.current_value)

    def next(self, value):
        super().next(value)
        self.current_value = value
