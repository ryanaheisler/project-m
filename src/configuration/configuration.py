import json
import os

from pyglet import resource

from src.utilities.reactive.subject import BehaviorSubject
from src.utilities.singleton_meta import Singleton


class Configuration(metaclass=Singleton):
    def __init__(self):
        self.__SHOW_FRAME_RATE = "show_frame_rate"
        self.__GAME_SPEED = "game_speed"
        self.__LOG_FILE_PATH = "log_file_path"

        with resource.file('config.json', 'r') as file_:
            self.settings = json.load(file_)

        speed_ = self.settings[self.__GAME_SPEED]
        self.__game_speed = BehaviorSubject(speed_)

    @property
    def should_show_frame_rate(self) -> bool:
        return self.settings[self.__SHOW_FRAME_RATE]

    @property
    def game_speed(self) -> BehaviorSubject:
        return self.__game_speed

    @game_speed.setter
    def game_speed(self, value: int):
        self.__game_speed.next(value)

    @property
    def log_file_path(self) -> str:
        return os.path.expanduser(self.settings[self.__LOG_FILE_PATH])
