import pyglet

from src.game_state.game_state_manager import GameStateManager
from src.utilities.logging.logger import log_error
from src.utilities.resources import application_resources


def main():
    try:
        pyglet.resource.path = application_resources
        pyglet.resource.reindex()

        GameStateManager()

        pyglet.app.run()

    except Exception as e:
        log_error("UNCAUGHT", e)
