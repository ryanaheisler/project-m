from pyglet import clock
from pyglet import window as pyglet_window_module
from pyglet.graphics import Batch
from pyglet.window import Window

from src.configuration.configuration import Configuration
from src.game_state.game_play import GamePlay
from src.input.input_handler import on_key_press, on_key_release
from src.input.temporary.game_speed_input_handler import game_speed_on_key_press
from src.utilities.command import Command
from src.utilities.main_window import main_window
from src.utilities.singleton_meta import Singleton


class GameStateManager(metaclass=Singleton):

    def __init__(self):
        _window: Window = main_window()

        sprite_batch = Batch()

        play_state = GamePlay(sprite_batch)
        clock.schedule_interval(play_state.tick, 1/60)

        on_draw = Command([_window.clear, sprite_batch.draw])

        if Configuration().should_show_frame_rate:
            fps_display = pyglet_window_module.FPSDisplay(window=_window)
            on_draw.add_instructions([fps_display.draw])

        _window.on_draw = on_draw
        _window.push_handlers(on_key_press, on_key_release)
        _window.push_handlers(on_key_press=game_speed_on_key_press)
