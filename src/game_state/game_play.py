from decimal import Decimal

from src.geometry.point import Point
from src.player.player import Player


class GamePlay:

    def __init__(self, sprite_batch):
        self.player = Player(sprite_batch=sprite_batch, position=Point(100, 0))

    def tick(self, delta_time_seconds):
        delta_time_milliseconds = Decimal(delta_time_seconds * 1000)
        self.player.tick(delta_time_milliseconds)
