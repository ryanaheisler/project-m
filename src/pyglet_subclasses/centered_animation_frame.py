from pyglet.image import AnimationFrame


class CenteredAnimationFrame(AnimationFrame):
    def __init__(self, image, duration):
        super().__init__(image, duration)
        self.image.anchor_x = self.image.width / 2
