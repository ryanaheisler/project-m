import cProfile
import os
import pstats
from pstats import SortKey
from select import select

from src.entrypoint import main

cProfile.run('main()', 'profile')

with open('profile.txt', 'w') as stream:
    p = pstats.Stats('profile', stream=stream)
    p.strip_dirs().sort_stats(SortKey.TIME).print_stats()

os.remove('profile')
