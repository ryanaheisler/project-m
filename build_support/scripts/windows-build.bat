@echo off
IF NOT EXIST %userprofile%\development\project-m (GOTO FAIL1)
cd %userprofile%\development\project-m
GOTO PROCEED

:FAIL1
echo failed to find development\project-m
IF NOT EXIST %userprofile%\project-m (GOTO FAIL2)
cd %userprofile%\project-m
GOTO PROCEED

:FAIL2
echo failed to find project-m
cd ..\..\

:PROCEED
rmdir /s /q build
rmdir /s /q dist
del /f /q "ProjectM*.spec"

:: Temporarily replace development files with ones needed for building
rename Pipfile Pipfile.bak
copy build_support\pipfiles\Windows-build.Pipfile Pipfile

copy support_files\config.json config.json.bak
del support_files\config.json
copy build_support\config_files\prototype_config.json support_files\config.json

:: Install dependencies and run pyinstaller
pipenv install --dev

git rev-parse @~ > temp_previous_commit.txt
set /p hash=<temp_previous_commit.txt

del temp_previous_commit.txt

set version=ProjectM_%DATE:~-4,4%%DATE:~-10,2%%DATE:~-07,2%_%hash:~0,7%_Win

:: -F = --onefile. Build the app as a single executable instead of a folder of files
:: -n = --name
:: -y = --noconfirm. Say yes to all confirmation prompts.
:: -w = --windowed, --noconsole. Don't open a console window when running the EXE
pyinstaller __main__.py -F -n %version% -w -y --add-data assets;assets --add-data support_files;support_files
pyinstaller -y %version%.spec

:: Put development files back and clean up
del Pipfile
rename Pipfile.bak Pipfile

del support_files\config.json
copy config.json.bak support_files\config.json
del config.json.bak

rmdir /s /q build