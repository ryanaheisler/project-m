cd ~/development/project-m/ || cd ~/project-m/ || cd ../../

rm -rf build
rm -rf dist
rm  -f ProjectM*.spec

# Temporarily replace development files with ones needed for building
mv Pipfile Pipfile.bak
cp build_support/pipfiles/Linux-build.Pipfile Pipfile

mv support_files/config.json config.json.bak
cp build_support/config_files/prototype_config.json support_files/config.json

# Install dependencies and run pyinstaller
pipenv install --dev

date=$(date +"%Y%m%d")
hash=$(git log --pretty=format:'%h' -n 1)
version=ProjectM_${date}_${hash}_Pop

# -F = --onefile. Build the app as a single executable instead of a folder of files
# -n = --name
# -y = --noconfirm. Say yes to all confirmation prompts.
pyinstaller __main__.py -F -n $version -y --add-data assets:assets --add-data support_files:support_files
pyinstaller -y $version.spec

# Put development files back
rm Pipfile
mv Pipfile.bak Pipfile

rm support_files/config.json
mv config.json.bak support_files/config.json

rm -rf build