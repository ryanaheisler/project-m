cd ~/development/project-m/ || cd ~/project-m/ || cd ../../

rm -rf build
rm -rf dist
rm  -f setup.py

# Temporarily replace development files with ones needed for building
mv Pipfile Pipfile.bak
cp build_support/pipfiles/OSX-build.Pipfile Pipfile

mv support_files/config.json config.json.bak
cp build_support/config_files/prototype_config.json support_files/config.json

# Install dependencies and run py2app
pipenv install --dev

date=$(date +"%Y%m%d")
hash=$(git log --pretty=format:'%h' -n 1)
version=ProjectM_${date}_${hash}_OSX

cp __main__.py $version.py
py2applet --make-setup $version.py

# --includes (-i)         comma-separated list of modules to include
# --iconfile              Icon file to use
# --resources (-r)        comma-separated list of additional data files and folders to include (not for code!)
python setup.py py2app -i pyglet -r assets,support_files --iconfile projectm.icns

# Put development files back and clean up
rm Pipfile
mv Pipfile.bak Pipfile

rm support_files/config.json
mv config.json.bak support_files/config.json

rm $version.py

rm -rf build