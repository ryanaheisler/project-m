# Project M

## About

Project M is a 2D platforming Metroidvania game written in python 3 using the [Pyglet OpenGL library](http://pyglet.org/).

## Installation

See the [wiki](https://bitbucket.org/ryanaheisler/project-m/wiki/Home) for instructions to set up a machine for development or building an executable.

## License

Art assets copyright 2021 [Zach Higgins](http://www.zachhiggins.com/about)

Everything else copyright 2021 [Ryan Heisler](www.ryanheisler.com)
